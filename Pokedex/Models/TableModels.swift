//
//  TableModels.swift
//  Pokedex
//
//  Created by Laurent Fernando Castañeda on 28/10/20.
//

import Foundation

struct PokemonInfo: Codable {
    let name: String
    let url: String
}

struct PokemonListModel: Codable {
    let count: Int
    var next: String?
    var previous: String?
    var results: [PokemonInfo]
}
