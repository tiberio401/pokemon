//
//  DetailModels.swift
//  Pokedex
//
//  Created by Laurent Fernando Castañeda on 28/10/20.
//

import Foundation

struct PokemonNameModel: Codable {
    let name: String
}

struct PokemonMoveModel: Codable{
    let move: PokemonNameModel
    let version_group_details: [PokemonMoveDetailModel]
}

struct PokemonMoveDetailModel: Codable {
    let level_learned_at: Int
    let move_learn_method: PokemonNameModel
}


struct PokemonStatsModel: Codable {
    let base_stat: Int
    let stat: PokemonNameModel
}

struct PokemonTypeModel: Codable{
    let type: PokemonNameModel
}


struct PokemonDetailModel: Codable {
    let stats: [PokemonStatsModel]
    let moves: [PokemonMoveModel]
    let species: PokemonNameModel
    let types: [PokemonTypeModel]
}
