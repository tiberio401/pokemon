//
//  PokeAPI.swift
//  Pokedex
//
//  Created by Laurent Fernando Castañeda on 28/10/20.
//

import Foundation

class PokeAPI {
    private static var url_base: String = "https://pokeapi.co/api/v2/"
    public static let image_url_base: String = "https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/"
    public static let image_avatar: String = "https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/official-artwork/"
    
    public static func GetPokemonList(next: String?) -> String{
        if next == nil {
            return self.url_base + "pokemon"
        }else{
            return next!
        }
    }
    
    public static func GetPokemonMoveList(next: String?) -> String{
        if next == nil {
            return self.url_base + "move"
        }else{
            return next!
        }
    }
    
    public static func GetPokemonDetail(id: Int) -> String{
        return self.url_base + "pokemon/\(id)/"
    }
}
