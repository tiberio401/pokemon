//
//  Message.swift
//  Pokedex
//
//  Created by Laurent Fernando Castañeda on 3/02/21.
//

import Foundation
import UIKit
import SwiftMessages

class MessageCustom{
    public static func ShowNoNetworkAlertMessage(){
        let view = MessageView.viewFromNib(layout: .statusLine)
        view.configureTheme(.warning)
        view.configureDropShadow()
        view.configureContent(title: "Atención", body: "No se ha podido conectar a internet")
        view.layoutMarginAdditions = UIEdgeInsets(top: 20, left: 20, bottom: 20, right: 20)

        (view.backgroundView as? CornerRoundingView)?.cornerRadius = 10

        SwiftMessages.show(view: view)
    }
}
