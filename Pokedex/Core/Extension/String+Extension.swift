//
//  String+Extension.swift
//  Pokedex
//
//  Created by Laurent Fernando Castañeda on 28/10/20.
//

import Foundation

extension String {
    func my_capitalizingFirstLetter() -> String {
        return prefix(1).capitalized + dropFirst()
    }
    mutating func my_capitalizingFirstLetter() {
        self = self.my_capitalizingFirstLetter()
    }
}
