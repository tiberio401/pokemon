//
//  PokemonMoveDetailTableViewCell.swift
//  Pokedex
//
//  Created by Laurent Fernando Castañeda on 29/10/20.
//

import UIKit

class PokemonMoveDetailTableViewCell: UITableViewCell {
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var levelLabel: UILabel!
    @IBOutlet weak var imagenType: UIImageView!
    
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func setValues(values: PokemonMoveModel){
        self.nameLabel.text = values.move.name.my_capitalizingFirstLetter()
        self.levelLabel.text = "Level \(values.version_group_details[0].level_learned_at)"
    }

}
