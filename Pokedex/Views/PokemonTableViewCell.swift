//
//  PokemonTableViewCell.swift
//  Pokedex
//
//  Created by Laurent Fernando Castañeda on 28/10/20.
//

import UIKit
import Kingfisher

class PokemonTableViewCell: UITableViewCell {
    
    @IBOutlet weak var avatar: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var codeLabel: UILabel!
    @IBOutlet weak var firstAttribute: UIImageView!
    @IBOutlet weak var secondsAttribute: UIImageView!
    

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    func setValues(pokemonInfo: PokemonInfo, index: Int){
        self.nameLabel.text = pokemonInfo.name.my_capitalizingFirstLetter()
        let imageId = pokemonInfo.url.replacingOccurrences(of: "https://pokeapi.co/api/v2/pokemon/", with: "")
        self.codeLabel.text = "#" + imageId.dropLast()        
        guard let url = URL(string: PokeAPI.image_url_base + imageId.dropLast() + ".png") else {
            return
        }
        self.avatar.kf.setImage(with: url)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
