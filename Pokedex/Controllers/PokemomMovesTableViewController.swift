//
//  PokemomMovesTableViewController.swift
//  Pokedex
//
//  Created by Laurent Fernando Castañeda on 30/10/20.
//

import UIKit

class PokemomMovesTableViewController: UITableViewController {

    var table: PokemonListModel?
    var filterPokemonMoves: [PokemonInfo] = []
    let searchController = UISearchController(searchResultsController: nil)
    var loadingAPI: Bool = false
    
    override func viewDidLoad() {
        
        navigationItem.title = "Moves"
        
        self.searchController.searchResultsUpdater = self
        self.searchController.obscuresBackgroundDuringPresentation = false
        self.searchController.searchBar.placeholder = "Buscar"
        self.tableView.tableHeaderView = self.searchController.searchBar
        
        self.loadInfo()
        super.viewDidLoad()

        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return self.filterPokemonMoves.count
        
    }
    
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath)
        cell.textLabel?.text = self.filterPokemonMoves[indexPath.row].name.my_capitalizingFirstLetter()
        return cell
    }
    
    
    override func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let offsetY = scrollView.contentOffset.y
        let contentHeight = scrollView.contentSize.height
        
        if offsetY > contentHeight - scrollView.frame.height {
            if !self.loadingAPI{
                self.loadInfo()
            }
        }
    }
    
    
    
    func loadInfo(){
        if Reachability.isConnectedToNetwork() == false{
            MessageCustom.ShowNoNetworkAlertMessage()
            return
        }
        if self.loadingAPI == true{
            return
        }
        self.loadingAPI = true
        
        guard let url = URL(string: PokeAPI.GetPokemonMoveList(next: self.table?.next)) else {
            return
        }
        var urlRequest = URLRequest(url: url)
        urlRequest.httpMethod = "GET"
        urlRequest.setValue("application/json", forHTTPHeaderField: "Content-Type")
        
        URLSession.shared.dataTask(with: urlRequest) { (data, response, error) in
            self.loadingAPI = false
            guard let data = data else {return}
            
            do{
                let jsonResponse = try JSONDecoder().decode(PokemonListModel.self, from: data)
                if self.table == nil {
                    self.table = jsonResponse
                }else{
                    self.table?.results.append(contentsOf: jsonResponse.results)
                    self.table?.next = jsonResponse.next
                }
                self.filterPokemonMoves = self.table!.results
                DispatchQueue.main.async {
                    self.searchController.searchBar.text = ""
                    self.tableView.reloadData()
                }
            }catch let error{
                print(error)
            }
        }.resume()
    }


}

extension PokemomMovesTableViewController: UISearchResultsUpdating {
    func updateSearchResults(for searchController: UISearchController) {
        if let data = self.table {
            self.filterPokemonMoves.removeAll(keepingCapacity: false)
            if self.searchController.searchBar.text! != ""{
                self.filterPokemonMoves = data.results.filter{$0.name.localizedCaseInsensitiveContains(self.searchController.searchBar.text!)}
            }else{
                self.filterPokemonMoves = data.results
            }
            
            self.tableView.reloadData()
            
        }
    }
}
