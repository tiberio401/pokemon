//
//  PokemonDetailViewController.swift
//  Pokedex
//
//  Created by Laurent Fernando Castañeda on 28/10/20.
//

import UIKit

class PokemonDetailViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {
    
    
    
    //Principal Info
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var principalView: UIView!
    @IBOutlet weak var segment: UISegmentedControl!
    
    
    @IBOutlet weak var avatarImage: UIImageView!
    @IBOutlet weak var firstTypeImage: UIImageView!
    @IBOutlet weak var secondsTypeImage: UIImageView!
    @IBOutlet weak var bodyView: UIView!
    @IBOutlet weak var statsView: UIView!
    @IBOutlet weak var moveView: UIView!
    
    
    //Stats View
    @IBOutlet weak var hpValueLabel: UILabel!
    @IBOutlet weak var atkValueLabel: UILabel!
    @IBOutlet weak var defValueLabel: UILabel!
    @IBOutlet weak var satkValueLabel: UILabel!
    @IBOutlet weak var sdefValueLabel: UILabel!
    @IBOutlet weak var spdValueLabel: UILabel!
    
    @IBOutlet weak var hpBarProgressBar: UIProgressView!
    @IBOutlet weak var atkProgressBar: UIProgressView!
    @IBOutlet weak var defBarProgressBar: UIProgressView!
    @IBOutlet weak var satkProgressBar: UIProgressView!
    @IBOutlet weak var sdefBarProgressBar: UIProgressView!
    @IBOutlet weak var spdProgressBar: UIProgressView!
    
    @IBOutlet weak var tableView: UITableView!
    
    
    
    var pokemonId: Int?
    var pokemonDetail: PokemonDetailModel?
    
    override func viewDidLoad() {
        navigationItem.title = "Detalle"
        
        let rectShape = CAShapeLayer()
        rectShape.bounds = self.bodyView.frame
        rectShape.position = self.bodyView.center
        rectShape.path = UIBezierPath(roundedRect: self.bodyView.bounds, byRoundingCorners: [.topLeft, .topRight, .bottomLeft], cornerRadii: CGSize(width: 50, height: 50)).cgPath
        
        self.bodyView.layer.mask = rectShape
        self.tableView.delegate = self
        self.tableView.dataSource = self
        
        self.loadDataFromAPI()
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
    }
    
    @IBAction func stepperValue(_ sender: UISegmentedControl) {
        switch sender.selectedSegmentIndex {
            case 0:
                self.statsView.isHidden = false
                self.moveView.isHidden = true
            case 1:
                self.statsView.isHidden = true
                self.moveView.isHidden = false
            case 2:
                self.statsView.isHidden = true
                self.moveView.isHidden = true
            default:
                print("error")
        }
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.pokemonDetail?.moves.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as? PokemonMoveDetailTableViewCell
        cell?.setValues(values: self.pokemonDetail!.moves[indexPath.row])
        return cell!
    }
    
    
    func setStatsValues(){
        guard let values = self.pokemonDetail?.stats else{ return }
        
        for value in values{
            switch value.stat.name {
            case "hp":
                self.hpValueLabel.text = String(value.base_stat)
                self.hpBarProgressBar.setProgress(Float(value.base_stat) / Float(100), animated: true)
            case "attack":
                self.atkValueLabel.text = String(value.base_stat)
                self.atkProgressBar.setProgress(Float(value.base_stat) / Float(100), animated: true)
            case "defense":
                self.defValueLabel.text = String(value.base_stat)
                self.defBarProgressBar.setProgress(Float(value.base_stat) / Float(100), animated: true)
            case "special-attack":
                self.satkValueLabel.text = String(value.base_stat)
                self.satkProgressBar.setProgress(Float(value.base_stat) / Float(100), animated: true)
            case "special-defense":
                self.sdefValueLabel.text = String(value.base_stat)
                self.sdefBarProgressBar.setProgress(Float(value.base_stat) / Float(100), animated: true)
            case "speed":
                self.spdValueLabel.text = String(value.base_stat)
                self.spdProgressBar.setProgress(Float(value.base_stat) / Float(100), animated: true)
            default:
                print("error")
            }
        }
    }
    
    func loadDataFromAPI(){
        if Reachability.isConnectedToNetwork() == false{
            MessageCustom.ShowNoNetworkAlertMessage()
            return
        }
        guard let pokemonId = self.pokemonId else {
            return
        }
        
        guard let url = URL(string: PokeAPI.GetPokemonDetail(id: pokemonId)) else {
            return
        }
        
        if let imageUrl = URL(string: PokeAPI.image_avatar + "\(pokemonId).png"){
            self.avatarImage.kf.setImage(with: imageUrl)
        }
        
        
        
        var urlRequest = URLRequest(url: url)
        urlRequest.httpMethod = "GET"
        urlRequest.setValue("application/json", forHTTPHeaderField: "Content-Type")
        
        URLSession.shared.dataTask(with: urlRequest) { (data, response, error) in
            guard let data = data else {return}
            
            do{
                self.pokemonDetail = try JSONDecoder().decode(PokemonDetailModel.self, from: data)
                DispatchQueue.main.async {
                    self.titleLabel.text = self.pokemonDetail!.species.name.my_capitalizingFirstLetter()
                    if self.pokemonDetail!.types.count == 2{
                        self.firstTypeImage.image = UIImage(named: "tag_" + self.pokemonDetail!.types[0].type.name)
                        self.firstTypeImage.isHidden = false
                        self.secondsTypeImage.image = UIImage(named: "tag_" + self.pokemonDetail!.types[1].type.name)
                        self.secondsTypeImage.isHidden = false
                    }else{
                        self.firstTypeImage.image = UIImage(named: "tag_" + self.pokemonDetail!.types[0].type.name)
                        self.firstTypeImage.isHidden = false
                    }
                    
                    let color = UIColor(named: "color_" + self.pokemonDetail!.types[0].type.name)
                    
                    self.principalView.backgroundColor = color
                    self.segment.selectedSegmentTintColor = color
                    
                    self.navigationController?.navigationBar.barTintColor = color
                    
                    // selected option color
                    self.segment.setTitleTextAttributes([NSAttributedString.Key.foregroundColor: UIColor.white], for: .selected)

                    // color of other options
                    self.segment.setTitleTextAttributes([NSAttributedString.Key.foregroundColor: color], for: .normal)
                    
                    self.setStatsValues()
                    self.tableView.reloadData()
                }
            }catch let error{
                print(error)
            }
        }.resume()
    }
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
}
