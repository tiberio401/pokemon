//
//  PokemonTableViewController.swift
//  Pokedex
//
//  Created by Laurent Fernando Castañeda on 28/10/20.
//

import UIKit
import SwiftMessages


class PokemonTableViewController: UITableViewController {
    
    var table: PokemonListModel?
    var filterPokemon: [PokemonInfo] = []
    let searchController = UISearchController(searchResultsController: nil)
    var loadingAPI: Bool = false
    
    override func viewDidLoad() {
        self.navigationItem.title = "Pokemon"

        self.searchController.searchResultsUpdater = self
        self.searchController.obscuresBackgroundDuringPresentation = false
        self.searchController.searchBar.placeholder = "Buscar Pokemones"
        self.tableView.tableHeaderView = self.searchController.searchBar
        
        self.loadInfo()
        super.viewDidLoad()
        
        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false
        
        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem
    }
    
    override func viewDidAppear(_ animated: Bool){
        self.navigationController?.navigationBar.barTintColor = UIColor.gray
        super.viewDidAppear(animated)
    }
    
    // MARK: - Table view data source
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return self.filterPokemon.count
        
    }
    
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as? PokemonTableViewCell
        cell?.setValues(pokemonInfo: self.filterPokemon[indexPath.row], index: indexPath.row)
        return cell!
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if self.tableView.indexPathForSelectedRow != nil{
            self.performSegue(withIdentifier: "detailSegue", sender: self)
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "detailSegue"{
            let imageId = self.filterPokemon[self.tableView.indexPathForSelectedRow!.row].url.replacingOccurrences(of: "https://pokeapi.co/api/v2/pokemon/", with: "")
            let pokemonDetail = segue.destination as! PokemonDetailViewController
            pokemonDetail.pokemonId = Int(imageId.dropLast())
        }
    }
    
    override func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let offsetY = scrollView.contentOffset.y
        let contentHeight = scrollView.contentSize.height
        let xx = scrollView.frame.height
        let yy = scrollView.frame.height
        if offsetY > contentHeight - scrollView.frame.height {
            if !self.loadingAPI{
                self.loadInfo()
            }
        }
    }
    
    
    
    func loadInfo(){
        if Reachability.isConnectedToNetwork() == false{
            MessageCustom.ShowNoNetworkAlertMessage()
            return
        }
        if self.loadingAPI == true{
            return
        }
        self.loadingAPI = true
        
        guard let url = URL(string: PokeAPI.GetPokemonList(next: self.table?.next)) else {
            return
        }
        print("entro")
        var urlRequest = URLRequest(url: url)
        urlRequest.httpMethod = "GET"
        urlRequest.setValue("application/json", forHTTPHeaderField: "Content-Type")
        
        URLSession.shared.dataTask(with: urlRequest) { (data, response, error) in
            self.loadingAPI = false
            
            if let response = response {
                
            }
            
            guard let data = data else {return}
            
            do{
                let jsonResponse = try JSONDecoder().decode(PokemonListModel.self, from: data)
                if self.table == nil {
                    self.table = jsonResponse
                }else{
                    self.table?.results.append(contentsOf: jsonResponse.results)
                    self.table?.next = jsonResponse.next
                }
                self.filterPokemon = self.table!.results
                DispatchQueue.main.async {
                    self.searchController.searchBar.text = ""
                    self.tableView.reloadData()
                }
            }catch let error{
                print(error)
            }
        }.resume()
    }    
}

extension PokemonTableViewController: UISearchResultsUpdating {
    func updateSearchResults(for searchController: UISearchController) {
        if let data = self.table {
            self.filterPokemon.removeAll(keepingCapacity: false)
            if self.searchController.searchBar.text! != ""{
                self.filterPokemon = data.results.filter{$0.name.localizedCaseInsensitiveContains(self.searchController.searchBar.text!)}
            }else{
                self.filterPokemon = data.results
            }
            
            self.tableView.reloadData()
            
        }
    }
}
